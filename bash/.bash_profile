# Set CLICOLOR if you want Ansi Colors in iTerm2 
export CLICOLOR=1

# Set colors to match iTerm2 Terminal Colors
export TERM=xterm-256color

[[ -s "$HOME/.rvm/scripts/rvm" ]] && source "$HOME/.rvm/scripts/rvm" # Load RVM into a shell session *as a function*

# Source bashrc
if [ -f ~/.bashrc ]; then source ~/.bashrc; fi

alias branchclean='git branch --merged | grep -v "\*" | grep -v master | grep -v dev | xargs -n 1 git branch -d'
alias flushdns='sudo killall -HUP mDNSResponder'
#alias ssh='ssh -v'

eval "$(pyenv init -)"
eval "$(pyenv virtualenv-init -)"
