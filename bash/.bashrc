PATH=/usr/local/bin:/usr/local/sbin:$PATH:$HOME/.rvm/bin # Add RVM to PATH for scripting

function _update_ps1() {
    PS1="$(~/go/bin/powerline-go -error $? -mode compatible -newline \
          	-modules "nix-shell,venv,ssh,cwd,perms,git,hg,jobs,exit,root,vgo,aws,docker" \
          )"
}

# Grep options
export GREP_OPTIONS='--color=auto'

# Custom bash prompt
#source ~/.bash_prompt

# AWS Credentials
[[ -f ~/.bash_aws ]] && . ~/.bash_aws

# DO Credentials
[[ -f ~/.bash_do ]] && . ~/.bash_do

# Bash History
export HISTFILESIZE=100000
shopt -s histappend
if [ "$TERM" != "linux" ]; then
    if [ -n "$PROMPT_COMMAND" ]; then
       PROMPT_COMMAND="_update_ps1; ${PROMPT_COMMAND}; history -a; history -n"
    else
       PROMPT_COMMAND="_update_ps1; history -a; history -n"
    fi
fi

if [[ -f "${HOME}/.config/cloudtoken/bashrc_additions" ]]; then
    source "${HOME}/.config/cloudtoken/bashrc_additions"
fi
